#ifndef DEFAULT_H
#define DEFAULT_H

/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#151515", // black
	"#ac4142", // red
	"#90a959", // green
	"#f4bf75", // yellow
	"#6a9fb5", // blue
	"#aa759f", // magenta
	"#75b5aa", // cyan
	"#d0d0d0", // white

	/* 8 bright colors */
	"#505050", // black
	"#ac4142", // red
	"#90a959", // green
	"#f4bf75", // yellow
	"#6a9fb5", // blue
	"#aa759f", // magenta
	"#75b5aa", // cyan
	"#f5f5f5", // white

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#d0d0d0", // foreground
	"#d0d0d0", // cursor
	"#111111", // background
};

#endif
